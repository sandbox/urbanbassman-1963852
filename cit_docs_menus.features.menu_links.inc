<?php
/**
 * @file
 * cit_docs_menus.features.menu_links.inc
 */

/**
 * Implementation of hook_menu_default_menu_links().
 */
function cit_docs_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:<front>
  $menu_links['main-menu:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: main-menu:publications/implementation-guides
  $menu_links['main-menu:publications/implementation-guides'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'publications/implementation-guides',
    'router_path' => 'publications/implementation-guides',
    'link_title' => 'Implementation Guides',
    'options' => array(
      'attributes' => array(
        'title' => 'Go to Implementation Guides',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-47',
  );
  // Exported menu link: main-menu:publications/new
  $menu_links['main-menu:publications/new'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'publications/new',
    'router_path' => 'publications/new',
    'link_title' => 'New!',
    'options' => array(
      'attributes' => array(
        'title' => 'New publications',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: main-menu:publications/system-administration-guides
  $menu_links['main-menu:publications/system-administration-guides'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'publications/system-administration-guides',
    'router_path' => 'publications/system-administration-guides',
    'link_title' => 'System Administration Guides',
    'options' => array(
      'attributes' => array(
        'title' => 'Go to System Administration Guides',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-46',
  );
  // Exported menu link: main-menu:publications/user-guides
  $menu_links['main-menu:publications/user-guides'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'publications/user-guides',
    'router_path' => 'publications/user-guides',
    'link_title' => 'User Guides',
    'options' => array(
      'attributes' => array(
        'title' => 'Go to User Guides',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Home');
  t('Implementation Guides');
  t('New!');
  t('System Administration Guides');
  t('User Guides');


  return $menu_links;
}
